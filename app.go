package main

import (
	"context"
	"fmt"

	"github.com/otiai10/opengraph"
)

// App struct
type App struct {
	ctx context.Context
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
}

func (a *App) FetchOpenGraphTags(postURL string) (map[string]string, error) {
	fmt.Println("FOGT:", postURL)
	og, err := opengraph.Fetch(postURL)
	fmt.Printf("OpenGraph: %+v\nError: %v\n", og, err)
	return map[string]string{
		"title":       og.Title,
		"description": og.Description,
		"url":         og.URL.String(),
	}, err
}

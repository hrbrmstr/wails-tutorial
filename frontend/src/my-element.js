import { css, html, LitElement, unsafeCSS } from 'lit'
import tachyonsCSS from '../node_modules/tachyons/css/tachyons.min.css?inline';
import { FetchOpenGraphTags } from "../wailsjs/go/main/App";

export class MyElement extends LitElement {

  static get properties() {
    return {
      resultText: { type: String },
      tags: { type: Object },
    }
  }

  constructor() {
    super()
    this.tags = {};
  }

  async fetchOG() {
    let thisURL = this.shadowRoot.querySelector('input#url').value
    console.log("fetchOG")
    FetchOpenGraphTags(thisURL).then(result => {
      console.log('fetch opengraph tags:', result)
      this.tags = result;
      this.requestUpdate();
    });
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
<div class="measure pa4">
    <label for="url" class="f6 b db mb2">Enter URL that has OG Tags</label>
    <input id="url" type="url" class="input-reset ba pa2 mb2 db w-100" autocomplete="off"
           value="https://bsky.app/profile/crushbort.bsky.social/post/3ki6bmyzcal2v">
    <button @click=${this.fetchOG} class="btn">Render</button>
  <div class="result" id="result">
    <pre wrap>${this.tags.title && this.tags.title}

${this.tags.description && this.tags.description}

${this.tags.url && this.tags.url}</pre>
  </div>
</div>
    `
  }

  static get styles() {
    return css`
    ${unsafeCSS(tachyonsCSS)}
    pre {
      white-space: pre-wrap;
    }
    `
  }

}

window.customElements.define('my-element', MyElement)
